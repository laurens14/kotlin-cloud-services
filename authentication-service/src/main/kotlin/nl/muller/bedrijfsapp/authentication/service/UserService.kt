package nl.muller.bedrijfsapp.authentication.service

import nl.muller.bedrijfsapp.authentication.model.UserLogin
import nl.muller.bedrijfsapp.authentication.model.request.LoginRequest
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient("user-service")
interface UserService {
    @RequestMapping(method = arrayOf(RequestMethod.POST), value = "/login", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun tryLogin(loginRequest: LoginRequest): UserLogin?
}


