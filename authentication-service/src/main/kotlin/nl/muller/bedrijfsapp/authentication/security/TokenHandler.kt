package nl.muller.bedrijfsapp.authentication.security

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import nl.muller.bedrijfsapp.authentication.security.data.UserAuthentication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.IOException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter

@Component
class TokenHandler @Autowired constructor(@Value("\${token.secret}") secret: String) {
    private val hmac: Mac

    init {
        try {
            val secretKey = DatatypeConverter.parseBase64Binary(secret)
            hmac = Mac.getInstance(HMAC_ALGO)
            hmac.init(SecretKeySpec(secretKey, HMAC_ALGO))

        } catch (e: NoSuchAlgorithmException) {
            throw IllegalStateException("failed to initialize HMAC: " + e.message, e)
        } catch (e: InvalidKeyException) {
            throw IllegalStateException("failed to initialize HMAC: " + e.message, e)
        }
    }

    fun parseUserFromToken(token: String): UserAuthentication? {
        val parts = token.split(SEPARATOR_SPLITTER.toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
        if (parts.size == 2 && parts[0].length > 0 && parts[1].length > 0) {
            try {
                val userBytes = fromBase64(parts[0])
                val hash = fromBase64(parts[1])

                val validHash = Arrays.equals(createHmac(userBytes), hash)
                if (validHash) {
                    val user = fromJSON(userBytes)

                    if (Date().time < user.expires) {
                        return user
                    }
                }
            } catch (e: IllegalArgumentException) {
                //log tempering attempt here
            }

        }
        return null
    }

    fun createTokenForUser(user: UserAuthentication): String {
        val userBytes = toJSON(user)
        val hash = createHmac(userBytes)
        val sb = StringBuilder(170)
        sb.append(toBase64(userBytes))
        sb.append(SEPARATOR)
        sb.append(toBase64(hash))
        return sb.toString()
    }

    private fun fromJSON(userBytes: ByteArray): UserAuthentication {
        try {
            return ObjectMapper().readValue<UserAuthentication>(ByteArrayInputStream(userBytes), UserAuthentication::class.java)
        } catch (e: IOException) {
            throw IllegalStateException(e)
        }

    }

    private fun toJSON(user: UserAuthentication): ByteArray {
        try {
            return ObjectMapper().writeValueAsBytes(user)
        } catch (e: JsonProcessingException) {
            throw IllegalStateException(e)
        }

    }

    private fun toBase64(content: ByteArray): String {
        return DatatypeConverter.printBase64Binary(content)
    }

    private fun fromBase64(content: String): ByteArray {
        return DatatypeConverter.parseBase64Binary(content)
    }

    // synchronized to guard internal hmac object
    @Synchronized private fun createHmac(content: ByteArray): ByteArray {
        return hmac.doFinal(content)
    }

    companion object {
        private val HMAC_ALGO = "HmacSHA256"
        private val SEPARATOR = "."
        private val SEPARATOR_SPLITTER = "\\."
    }

    /*
	public static void main(String[] args) {
		Date start = new Date();
		byte[] secret = new byte[70];
		new java.security.SecureRandom().nextBytes(secret);

		TokenHandler tokenHandler = new TokenHandler(secret);
		for (int i = 0; i < 1000; i++) {
			final User user = new User(java.util.UUID.randomUUID().toString().substring(0, 8), new Date(
					new Date().getTime() + 10000));
			user.grantRole(UserRole.ADMIN);
			final String token = tokenHandler.createTokenForUser(user);
			final User parsedUser = tokenHandler.parseUserFromToken(token);
			if (parsedUser == null || parsedUser.getUsername() == null) {
				System.out.println("error");
			}
		}
		System.out.println(System.currentTimeMillis() - start.getTime());
	}
*/
}
