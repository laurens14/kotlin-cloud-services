package nl.muller.bedrijfsapp.authentication.controller

import nl.muller.bedrijfsapp.authentication.model.request.LoginRequest
import nl.muller.bedrijfsapp.authentication.security.TokenHandler
import nl.muller.bedrijfsapp.authentication.security.data.UserAuthentication
import nl.muller.bedrijfsapp.authentication.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*

@RestController
class AuthenticationController @Autowired constructor(val tokenHandler : TokenHandler, val userService : UserService) {
    val passwordEncoder : BCryptPasswordEncoder = BCryptPasswordEncoder()
    private val TEN_DAYS = 1000 * 60 * 60 * 24 * 10.toLong()

    @RequestMapping(value = "/encode", method = arrayOf(RequestMethod.POST))
    fun getToken(@RequestBody userAuthentication: UserAuthentication) : String {
        return tokenHandler.createTokenForUser(userAuthentication)
    }

    @RequestMapping(value = "/decode", method = arrayOf(RequestMethod.POST))
    fun getAuthenticatedUser(@RequestBody token : String) : ResponseEntity<UserAuthentication> {
        tokenHandler.parseUserFromToken(token)?.let { userAuthentication ->
            return ResponseEntity(userAuthentication, HttpStatus.OK)
        }

        return ResponseEntity(null, HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @RequestMapping(value = "/login", method = arrayOf(RequestMethod.POST))
    fun login(@RequestBody loginRequest: LoginRequest): ResponseEntity<String> {
        //TODO: move login to user service (same for sign Up) ? just redirect from here so we can make token here,
        //TODO: OR call this method from user service? > YE > Create method: createTokenForUser() and call it from userService
        //TODO: this means we get /user/login instead of /authenticate/login
        val user = userService.tryLogin(loginRequest)
//
    //        //TODO: move to service
    //        user?.let {
    //            if (passwordEncoder.matches(loginRequest.password, user.password)) {
    //                user.expires = System.currentTimeMillis() + TEN_DAYS
    //
    //                val token = tokenHandler.createTokenForUser(user)
    //
    //                return ResponseEntity(token, HttpStatus.ACCEPTED);
    //            } else {
    //                return ResponseEntity(null, HttpStatus.UNPROCESSABLE_ENTITY);
    //            }
    //        }

        return ResponseEntity(null, HttpStatus.NOT_FOUND)
    }

    @RequestMapping(value = "/authenticate", method = arrayOf(RequestMethod.POST))
    @ResponseBody
    fun authenticate(@RequestBody token: String) : ResponseEntity<Long> {
        tokenHandler.parseUserFromToken(token)?.let { user -> return ResponseEntity(user.id, HttpStatus.ACCEPTED); }

        return ResponseEntity(null, HttpStatus.BAD_REQUEST)
    }
}