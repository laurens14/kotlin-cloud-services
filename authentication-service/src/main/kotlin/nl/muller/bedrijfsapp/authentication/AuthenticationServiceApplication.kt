package nl.muller.bedrijfsapp.authentication

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.feign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.filter.CharacterEncodingFilter
import javax.servlet.Filter

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan
open class AuthenticationServiceApplication {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(AuthenticationServiceApplication::class.java, *args)
        }

        @Bean
        fun characterEncodingFilter(): Filter {
            val characterEncodingFilter = CharacterEncodingFilter()
            characterEncodingFilter.setEncoding("UTF-8")
            characterEncodingFilter.setForceEncoding(true)
            return characterEncodingFilter
        }
    }
}