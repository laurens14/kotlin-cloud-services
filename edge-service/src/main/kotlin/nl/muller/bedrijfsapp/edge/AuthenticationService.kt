package nl.muller.bedrijfsapp.edge

import nl.muller.bedrijfsapp.edge.security.data.UserAuthentication
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient("authentication-service")
interface AuthenticationService {
    @RequestMapping(method = arrayOf(RequestMethod.POST), value = "/decode", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getAuthenticatedUser(token: String): UserAuthentication?

    @RequestMapping(method = arrayOf(RequestMethod.POST), value = "/authorize", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun checkUserAccess(urlPath: String): Boolean
}

