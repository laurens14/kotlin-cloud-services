package nl.muller.bedrijfsapp.edge

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.feign.EnableFeignClients
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableZuulProxy
open class EdgeApplication {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(EdgeApplication::class.java, *args)
        }
    }
}
