package nl.muller.bedrijfsapp.user.data

import  com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "User", uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("email"))))
class User {
    constructor() {
    }

    constructor(email: String) {
        this.email = email
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long = 0

    @NotNull
    @Size(min = 4, max = 30)
    public var email: String? = null

    @NotNull
    @Size(min = 4, max = 100)
    @JsonIgnore
    public var password: String? = null

    @Transient
    var expires: Long = 0

    override fun toString(): String {
        return javaClass.simpleName + ": " + email
    }
}
