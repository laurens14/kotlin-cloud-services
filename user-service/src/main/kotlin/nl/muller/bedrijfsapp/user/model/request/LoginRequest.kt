package nl.muller.bedrijfsapp.user.model.request;

class LoginRequest {
    var email: String = ""
    var password: String = ""
}
