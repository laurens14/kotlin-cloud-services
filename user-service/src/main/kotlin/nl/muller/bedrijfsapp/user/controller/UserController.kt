package nl.muller.bedrijfsapp.user.controller

import nl.muller.bedrijfsapp.user.data.User
import nl.muller.bedrijfsapp.user.data.UserRepository
import nl.muller.bedrijfsapp.user.model.request.LoginRequest
import nl.muller.bedrijfsapp.user.service.AuthenticationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*

@RestController
class UserController @Autowired constructor(val userRepository: UserRepository, val authenticationService: AuthenticationService) {
    val passwordEncoder : BCryptPasswordEncoder = BCryptPasswordEncoder()
    private val TEN_DAYS = 1000 * 60 * 60 * 24 * 10.toLong()

    @Autowired
    private var loadBalancer : LoadBalancerClient? = null

    @RequestMapping(value = "/login", method = arrayOf(RequestMethod.POST))
    fun login(@RequestBody loginRequest: LoginRequest): ResponseEntity<String> {
        val user = userRepository.findByEmail(loginRequest.email)

        var service = loadBalancer!!.choose("authentication-service")

        println("JOJOJOJOJOJOJO")

        if (service != null) {
            println(service.uri)
        }


        //TODO: move to service?
        user?.let {
            if (passwordEncoder.matches(loginRequest.password, user.password)) {
                user.expires = System.currentTimeMillis() + TEN_DAYS

                val token = authenticationService.getTokenForUser(user)

                if (StringUtils.isEmpty(token)) {
                    return ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR)
                }

                return ResponseEntity(token, HttpStatus.ACCEPTED);
            } else {
                return ResponseEntity(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }

        return ResponseEntity(null, HttpStatus.NOT_FOUND)
    }

    @RequestMapping(value = "/", method = arrayOf(RequestMethod.GET))
    fun me(@RequestHeader("USER_ID") userId : Long): ResponseEntity<User> {
        userRepository.findById(userId)?.let {
            user -> return ResponseEntity(user, HttpStatus.OK)
        }

        return ResponseEntity(null, HttpStatus.NOT_FOUND)
    }

    @RequestMapping(value = "/hallomensen", method = arrayOf(RequestMethod.GET))
    @ResponseBody
    fun hallo(@RequestHeader("USER_ID") userId : Long) : String {
        return "Hallo " + userRepository.findById(userId)?.email;
    }
}