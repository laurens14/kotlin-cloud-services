package nl.muller.bedrijfsapp.user

import nl.muller.bedrijfsapp.user.data.User
import nl.muller.bedrijfsapp.user.data.UserRepository
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.feign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.filter.CharacterEncodingFilter
import javax.servlet.Filter

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan
open class UserServiceApplication {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(UserServiceApplication::class.java, *args)
        }

        @Bean
        fun insertDefaultUsers(): InitializingBean {
            return object : InitializingBean {
                @Autowired
                private val userRepository: UserRepository? = null

                override fun afterPropertiesSet() {
                    addUser("admin@admin.com", "admin")
                    addUser("user@user.com", "user")
                }

                private fun addUser(username: String, password: String) {
                    val user = User()
                    user.email = username
                    user.password = BCryptPasswordEncoder().encode(password)
//                    user.grantRole(if (username == "admin") UserRole.ADMIN else UserRole.USER)
                    userRepository!!.save(user)
                }
            }
        }

        @Bean
        fun characterEncodingFilter(): Filter {
            val characterEncodingFilter = CharacterEncodingFilter()
            characterEncodingFilter.setEncoding("UTF-8")
            characterEncodingFilter.setForceEncoding(true)
            return characterEncodingFilter
        }
    }
}