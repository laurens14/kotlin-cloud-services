package nl.muller.bedrijfsapp.user.service

import nl.muller.bedrijfsapp.user.data.User
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod


@FeignClient("authentication-service")
interface AuthenticationService {
    @RequestMapping(method = arrayOf(RequestMethod.POST), value = "/encode", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getTokenForUser(user: User): String
}